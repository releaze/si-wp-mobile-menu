<?php
global $wpdb;

$nav_menus          = wp_get_nav_menus();
$menu_count         = count( $nav_menus );
$mobileMenuEntities = json_decode( si_ab_mobile_menu_get_db_value( 'mobile_menu_entities' ), true );
$isDisableTabs      = si_ab_mobile_menu_get_db_value( 'mobile_menu_disable_tabs' ) == 'true';
?>

<div class="si-ab-mobile-menu-settings">
	<h1>SI AB Mobile Menu</h1>
	<h4><?php echo esc_html__( 'Select the menus to be displayed in the mobile menu', 'si_ab_mobile_menu' );?></h4>

	<?php
	    $isSuccessfully = true;

	    //Click on Save settings button
	    if ( !empty( $_POST ) && $_POST['action'] == 'import-menu-settings' ) {
	        $disableTabs  = !empty( $_POST['disable_tabs'] );
            $menuEntities = array();

            $i = 0;
            while ($i < $menu_count) {
                $i++;
                $tabName = $_POST['mobile_menu_tab_' . $i];
                $menuName = $_POST['mobile_menu_' . $i];

                if (!$tabName || !$menuName) continue;
                $menuEntities['menu_' . $i] = array($tabName, $menuName);
            }

            si_ab_mobile_menu_set_db_value( 'mobile_menu_entities', json_encode( $menuEntities ) );
            si_ab_mobile_menu_set_db_value( 'mobile_menu_disable_tabs', json_encode($disableTabs) );

            if ( $isSuccessfully ) {
                echo '<div><h3 class="successful-text">Successful</h3></div>';
                $isSuccessfully = false;
            } else {
                echo '<div><h3 class="error-text">Something went wrong!</h3></div>';
            }
        }
	?>

    <?php if ($isSuccessfully) { ?>
        <form class="si-ab-mobile-menu-settings__form" method="POST">
            <?php
                $i = 0;
                while ($i < $menu_count) {
                    $i++;
                    ?>
                        <h3>
                            <?php echo esc_html__( 'Menu', 'si_ab_mobile_menu' )?>
                            <?php echo $i;?>
                        </h3>
                        <div class="si-ab-mobile-menu-settings__row">
                            <div>
                                <input type="text" value="<?php printf( $mobileMenuEntities['menu_' . $i][0] ) ?>" name=<?php echo "mobile_menu_tab_" . $i;?> placeholder="Tab name"/>
                            </div>
                            <div>
                                <select name=<?php echo "mobile_menu_" . $i;?>>
                                    <option value="0"><?php printf( '&mdash; %s &mdash;', esc_html__( 'Select a Menu' ) ); ?></option>
                                    <?php
                                        foreach ( $nav_menus as $menu ) :
                                            $data_orig = '';
                                            $selected = isset( $mobileMenuEntities['menu_' . $i][1] ) && $mobileMenuEntities['menu_' . $i][1] == $menu->slug;

                                            if ( $selected ) {
                                                $data_orig = 'data-orig="true"';
                                            }
                                            ?>
                                            <option <?php echo $data_orig; ?> <?php selected( $selected ); ?> value="<?php echo $menu->slug; ?>">
                                                <?php echo wp_html_excerpt( $menu->name, 40, '&hellip;' ); ?>
                                            </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    <?php
                }
            ?>

            <div class="si-ab-mobile-menu-checkbox-row">
                <input type="checkbox" name="disable_tabs" <?php printf( $isDisableTabs ? 'checked' : '' ); ?>>
                <h4><?php echo esc_html__( 'Disable tabs', 'si_ab_mobile_menu' )?></h4>
            </div>

            <p>
                <button type="submit" class="button button-primary button-large">Save settings</button>
                <input type="text" name="action" value="import-menu-settings" hidden/>
            </p>
        </form>
    <?php } ?>
</div>

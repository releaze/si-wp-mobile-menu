
(function() {
    window.onload = function() {
        const menuHeaderHolder = document.querySelector(".si-ab-mobile-menu-header-holder");
        const toggleButton = menuHeaderHolder.querySelector(".si-ab-mobile-menu-toggle");

        const tabs = document.querySelector('.si-ab-mobile-menu-header-holder .mobile-tabs ul');

        if (tabs) {
            const li = tabs.getElementsByTagName("li");

            for (let i = 0; i < li.length; i++) {
                li[i].addEventListener("click", function(event) {
                    event.stopPropagation();

                    if (event.target.tagName === 'LI') {
                        const current = document.querySelector(".active");
                        current.classList.remove('active');
                        this.classList.add('active');

                        const activeMenuName = `menu-${i}`;
                        const prevLinksMenu = document.querySelector(".show");
                        prevLinksMenu.classList.remove('show');

                        const activeMenu = document.querySelector(`.${activeMenuName}`);
                        activeMenu.classList.add('show');
                    }
                });
            }
        }

        toggleButton.onclick = function(){
            menuHeaderHolder.classList.toggle("toggled");
        }


        document.addEventListener('click', event => {
            const target = event.target;
            const isMenu = target === menuHeaderHolder || menuHeaderHolder.contains(target);

            if (!isMenu) {
                menuHeaderHolder.classList.remove("toggled");
            }
        })
    }
})();





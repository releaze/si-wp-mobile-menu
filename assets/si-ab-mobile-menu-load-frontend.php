<?php
global $wpdb;
global $post;
$pageID             = $post->post_name;
$site_title         = get_bloginfo( 'name' );
$home_page_url      = get_home_url();
$mobileMenuEntities = json_decode( si_ab_mobile_menu_get_db_value( 'mobile_menu_entities' ), true );
$isDisableTabs      = si_ab_mobile_menu_get_db_value( 'mobile_menu_disable_tabs' ) == 'true';
?>

<div class="si-ab-mobile-menu-header-holder">
   <a class="si-ab-mobile-menu-toggle"><span><em></em></span></a>
   <div class="si-ab-mobile-menu-logo-holder">
        <a href=<?php echo $home_page_url; ?>>
            <?php echo $site_title; ?>
        </a>
   </div>

   <div class="si-ab-mobile-menu-left-panel">
       <?php if (!$isDisableTabs) { ?>
            <div class="mobile-tabs">
               <ul>
                <?php
                    $index = 0;
                    foreach ($mobileMenuEntities as $key => $value) {
                        $className = $index == 0 ? 'active' : '';
                        $index++;
                    ?>
                        <li class=<?php echo $className; ?>><?php echo $value[0]; ?></li>
                    <?php
                    }
                    ?>
                ?>
               </ul>
           </div>
       <?php } ?>

       <div class="mobile-menu">
           <?php
                $i = 0;
                foreach ($mobileMenuEntities as $key => $value) {
                    $className = $i == 0 ? 'menu-' . $i . ' show' : 'menu-' . $i;

                    wp_nav_menu(
                       array(
                           'menu'       => $value[1],
                           'menu_class' => 'mobile-menu-ul ' . $className,
                        )
                    );

                    $i++;
                }
            ?>
      </div>
   </div>
</div>

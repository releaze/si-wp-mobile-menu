<?php
/**
    * Plugin Name: SI AB Mobile Menu
    * Description: Add a mobile menu with tabs
    * Version: 1.0
    * Requires at least: 5.8
    * Author:      Sports Innovation
    * Author URI:  https://sportsinnovation.dk
    * License:     GPLv2 or later
    * License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
    * Domain Path: /languages
    * Text Domain: si_ab_mobile_menu
*/
if (!defined('ABSPATH')) {
	die;
}

global $wpdb;
define( 'SI_AB_MOBILE_MENU_SQL_TABLE_NAME', $wpdb->prefix . 'si_ab_mobile_menu' );

/**
 * Register custom Mobile Menu page
 */
function si_ab_mobile_menu_register_custom_page() {
    add_menu_page(
        esc_html__( 'Welcome to plugin page', 'si_ab_mobile_menu' ),
        esc_html__( 'Mobile Menu', 'si_ab_mobile_menu' ),
        'manage_options',
        'si_ab_mobile_menu_options',
        'si_ab_mobile_menu_show_settings',
        'dashicons-smartphone',
        81
    );
}
add_action('admin_menu', 'si_ab_mobile_menu_register_custom_page');

/**
 * Settings page body
 */
function si_ab_mobile_menu_show_settings() {
    require plugin_dir_path( __FILE__ ) . 'assets/si-ab-mobile-menu-settings-page.php';
}

/**
 * Load css for admin menu setting page
 */

function si_ab_mobile_menu_load_assets($hook_suffix) {
    if ($hook_suffix != 'toplevel_page_si_ab_mobile_menu_options') return;

    wp_enqueue_style( 'si_ab_mobile_menu_styles', plugins_url( 'assets/css/admin.css', __FILE__ ) );
}
add_action( 'admin_enqueue_scripts', 'si_ab_mobile_menu_load_assets', 99 );

/**
 * Load css and js for frontend
 */
function si_ab_mobile_load_frontend_assets() {
    wp_enqueue_style( 'si_ab_mobile_menu_styles', plugins_url( 'assets/css/mobmenu.css', __FILE__ ) );
    wp_enqueue_script('si_ab_mobile_menu_scripts', plugins_url( 'assets/js/si-ab-mobile-menu.js', __FILE__ ));
}
add_action( 'wp_enqueue_scripts', 'si_ab_mobile_load_frontend_assets', 100 );

/**
 * Downloading language packs
 */
function si_ab_mobile_menu_load_textdomain() {
    load_plugin_textdomain( 'si_ab_mobile_menu', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
add_action( 'init', 'si_ab_mobile_menu_load_textdomain' );

/**
 * Load menu HTML to body
 */
function si_ab_mobile_menu_load_html_markup() {
    require plugin_dir_path( __FILE__ ) . 'assets/si-ab-mobile-menu-load-frontend.php';
}
add_action( 'wp_footer', 'si_ab_mobile_menu_load_html_markup' );

/**
 * Helpers
 */
require plugin_dir_path( __FILE__ ) . 'helpers.php';

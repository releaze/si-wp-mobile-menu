<?php
/**
 * Create db table
 */
function si_ab_mobile_menu_create_db_table() {
	global $wpdb;
	$table_name = constant( 'SI_AB_MOBILE_MENU_SQL_TABLE_NAME' );
	$charset_collate  = $wpdb->get_charset_collate();
	$sql = "CREATE TABLE $table_name (
		key_name  VARCHAR(255) NOT NULL,
		value     TEXT NOT NULL
	) $charset_collate;";
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}

/**
 * Get value in db
 *
 * @param string $key db key.
 *
 * @return mixed
 */
function si_ab_mobile_menu_get_db_value( $key ) {
	global $wpdb;
	$table_name = constant( 'SI_AB_MOBILE_MENU_SQL_TABLE_NAME' );
	$query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );

  if ( ! $wpdb->get_var( $query ) == $table_name ) {
		si_ab_mobile_menu_create_db_table();
  }

	return $wpdb->get_var( "SELECT value FROM $table_name WHERE key_name = '$key'" );
}

/**
 * Set value in db
 *
 * @param string $key db key.
 * @param string $value new value.
 *
 * @return mixed
 */
function si_ab_mobile_menu_set_db_value( $key, $value ) {
    global $wpdb;
    $table_name = constant( 'SI_AB_MOBILE_MENU_SQL_TABLE_NAME' );
    $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );

    if ( ! $wpdb->get_var( $query ) == $table_name ) {
        si_ab_mobile_menu_create_db_table();
    }

    if ( empty( $wpdb->get_results( "SELECT value FROM $table_name WHERE key_name = '$key'" ) ) ) {
        return $wpdb->insert( $table_name, array( 'key_name' => $key, 'value' => $value ) );
    }

    return $wpdb->update( $table_name, array( 'value' => $value ), array( 'key_name' => $key ) );
}
